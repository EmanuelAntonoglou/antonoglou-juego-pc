extends Node2D
class_name Ball

@export var speed = 400
@export var currentColor: BrickColor = BrickColor.Blue
enum BrickColor {
	None,
	Blue,
	Green,
	Yellow,
	Orange,
	Red,
	Purple }

var velocidad_constante = 400
var max_velocity: int = 700
var started: bool = false
var recover_life: int = 0
var divide: int = 0
var times_divided: int = 0
var times_can_divide: int = 2
var game

@onready var RB = $RigidBody2D
@onready var sprite = $RigidBody2D/Sprite
@onready var animation = $RigidBody2D/Sprite/AnimationPlayer
@onready var APBounce = $APBounce

func _physics_process(_delta):
	var velocidad_actual = RB.linear_velocity
	#velocidad_actual.reflect(velocidad_actual.normalized())
	var direccion = velocidad_actual.normalized()
	RB.linear_velocity = velocidad_constante * direccion

func SetPosition(pos):
	position = Vector2(100, 100)
	position = pos

func StartBall():
	started = true
	randomize()
	var random = randf_range(-100, 100)
	RB.linear_velocity = Vector2(random, -speed)

func ChangeColor(color: int):
	sprite.frame_coords.y = color - 1
	currentColor = color as BrickColor
	if currentColor == BrickColor.Yellow:
		ChangeVelocity(440)
	elif currentColor == BrickColor.Orange:
		ChangeScale(2.5)
		ChangeVelocity(200)
	elif currentColor == BrickColor.Purple:
		ChangeVelocity(420)
		ChangeCollisionMask(5, false)

func ChangeVelocity(new_speed: int):
	velocidad_constante = new_speed
	speed = new_speed

func SpeedUp(new_speed: int):
	if not speed >= max_velocity:
		velocidad_constante += new_speed
		speed += new_speed

func ChangeScale(new_scale: float):
	var newScale = Vector2(new_scale, new_scale)
	sprite.scale = newScale
	$RigidBody2D/CollisionShape2D.scale = newScale
	$RigidBody2D/Area2D/CollisionShape2D.scale = newScale

func SizeUp(new_scale: int):
	var newScale = Vector2(new_scale, new_scale)
	sprite.scale += newScale
	$RigidBody2D/CollisionShape2D.scale += newScale
	$RigidBody2D/Area2D/CollisionShape2D.scale += newScale

func ChangeCollisionMask(mask:int, activate:bool):
	RB.set_collision_mask_value(mask, activate)

func TryToRecoverLife():
	if recover_life < 3:
		recover_life += 1
	elif recover_life == 3:
		recover_life = 0
		SpeedUp(75)
		game.RecoverLife()

func TryToDivide():
	if times_divided != times_can_divide:
		if divide < 3:
			divide += 1
		elif divide == 3:
			divide = 0
			times_divided += 1
			if times_divided == 1:
				SpeedUp(25)
				ChangeScale(1.75)
			elif times_divided == 2:
				SpeedUp(50)
				ChangeScale(1.20)
			InstantiateOrangeBall()

func InstantiateOrangeBall():
	game.paddle.InstantiateBallPaddle()
	game.paddle.MoveBallToGame()
	game.paddle.balls[game.paddle.balls.size()-1].ChangeColor(BrickColor.Orange)
	game.paddle.balls[game.paddle.balls.size()-1].times_divided = times_divided
	ChangeScaleAndSpeed()
	game.paddle.balls[game.paddle.balls.size()-1].StartBall()

func ChangeScaleAndSpeed():
	if game.paddle.balls[game.paddle.balls.size()-1].times_divided == 1:
		game.paddle.balls[game.paddle.balls.size()-1].SpeedUp(25)
		game.paddle.balls[game.paddle.balls.size()-1].ChangeScale(1.75)
	elif game.paddle.balls[game.paddle.balls.size()-1].times_divided == 2:
		game.paddle.balls[game.paddle.balls.size()-1].SpeedUp(50)
		game.paddle.balls[game.paddle.balls.size()-1].ChangeScale(1.20)

func _on_area_2d_area_entered(area):
	if area.is_in_group("TakeDamage"):
		if not game.win:
			game.paddle.RemoveBall()
			if game.paddle.balls.size() == 0:
				game.TakeDamage()
		queue_free()
	if area.get_parent().is_in_group("ChangeDirection"):
		#var velocidad_actual = RB.linear_velocity
		#var direccion = velocidad_actual.normalized() * -100
		APBounce.play()
		pass
	if started:
		RB.linear_velocity += Vector2(100, 100)
