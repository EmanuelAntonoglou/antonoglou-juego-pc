extends Node2D

var ram: float = 0
var maxRam: float = 200
var won_spaceship: bool = false
var crashed: bool = true

var level
var taskbar
var startButton

var mouse_arrow = preload("res://Images/Window/Mouse Normal.png")
var mouse_hover = preload("res://Images/Window/Mouse Click.png")
var mouse_text = preload("res://Images/Window/Mouse Text.png")
var new_hotspot = Vector2()

func _ready():
	Input.set_custom_mouse_cursor(mouse_arrow, Input.CURSOR_ARROW, Vector2(32, 38))

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == MOUSE_BUTTON_LEFT:
			AudioManager.ChangeVolume("Click", 18)
			AudioManager.Play("Click")

func _physics_process(_delta):
	var mouse_position = get_viewport().get_mouse_position()
	if not get_viewport_rect().has_point(mouse_position):
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func ChangeMouse(cursor: String):
	if cursor == "Arrow":
		Input.set_custom_mouse_cursor(mouse_arrow, Input.CURSOR_ARROW, Vector2(32, 38))
	elif cursor == "Hover":
		Input.set_custom_mouse_cursor(mouse_hover, Input.CURSOR_ARROW, Vector2(32, 38))
	elif cursor == "Text":
		Input.set_custom_mouse_cursor(mouse_text, Input.CURSOR_IBEAM, Vector2(32, 38))
