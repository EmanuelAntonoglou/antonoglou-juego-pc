extends Node2D

var audios: Array[Sound] = []
var audioPlayers = {}

var resAudiosTyping: ResourceGroup = load("res://ResourceGroup/Audio/typingSounds.tres")
var resMusic: ResourceGroup = load("res://ResourceGroup/Audio/music.tres")
var resSFX: ResourceGroup = load("res://ResourceGroup/Audio/sfx.tres")

var musicPlayerSong: int = 0
var musicPlayerVolume: float = 0
var musicPlayerPosition: float = 0
var audiosTyping: Array[String] = []
var ignoreInTyping: Array[String] = ["Typing Enter"]
var dontIgnore: Array[String] = []


func Ready():
	AddResToAudioList(resAudiosTyping)
	AddResToAudioList(resMusic)
	AddResToAudioList(resSFX)
	CreateAudioPlayers()
	AppendToAudioList(resAudiosTyping, audiosTyping, ignoreInTyping)

func AddResToAudioList(res: ResourceGroup):
	for audioPath in res.paths:
		var fileName = audioPath.split("/")[-1].split(".")[0]
		var canAddAudio = 0
		for audio in audios:
			if fileName != audio.name:
				canAddAudio += 1
		if canAddAudio == audios.size():
			var newSound = Sound.new()
			newSound.SetValues(fileName, load(audioPath))
			audios.append(newSound)

func AppendToAudioList(res: ResourceGroup, audioList: Array[String], ignoreList: Array[String]):
	for audioPath in res.paths:
		var fileName = audioPath.split("/")[-1].split(".")[0]
		if fileName not in ignoreList:
			audioList.append(fileName)

func CreateAudioPlayers():
	for audio in audios:
		CreateAudioStreamPlayer(audio.name);
		SetValuesToPlayer(audio);
	CreateAudioStreamPlayer("typing");

func CreateAudioStreamPlayer(apName: String):
	var ap = AudioStreamPlayer.new()
	ap.name = apName
	add_child(ap)
	audioPlayers[ap.name] = ap
	audioPlayers[ap.name].stream = GetAudio(ap.name)

func SetValuesToPlayer(audio: Sound):
	audioPlayers[audio.name].volume_db = audio.volume
	audioPlayers[audio.name].pitch_scale = audio.pitch
	audioPlayers[audio.name].autoplay = audio.autoplay

func GetAudio(audioName) -> AudioStreamMP3:
	for audio in audios:
		if audio.name == audioName:
			return audio.file
	return null

func Play(apName: String, audioPosition: float = 0):
	if audioPlayers.has(apName):
		if audioPosition > 0.0:
			audioPlayers[apName].play(audioPosition)
		else:
			audioPlayers[apName].play()

func Pause(apName: String):
	if audioPlayers.has(apName):
		audioPlayers[apName].stop()

func GetPosition(apName: String) -> float:
	if audioPlayers.has(apName):
		return audioPlayers[apName].get_playback_position()
	return 0

func ChangeVolume(apName: String, volume: float = 0):
	if audioPlayers.has(apName):
		audioPlayers[apName].volume_db = volume

func PlayRandomTypingSound():
	var random_index = randi_range(0, audiosTyping.size() - 1)
	var random_sound = audiosTyping[random_index]
	audioPlayers["typing"].stream = GetAudio(random_sound)
	Play("typing")

