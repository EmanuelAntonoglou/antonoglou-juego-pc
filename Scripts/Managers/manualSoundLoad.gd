extends Node2D
@export var audiosManual: Array[Sound] = []

func _ready():
	for audio in audiosManual:
		audio.SetName()
	AudioManager.audios.append_array(audiosManual)
	AudioManager.Ready()
	queue_free()
