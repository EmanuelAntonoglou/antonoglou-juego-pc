extends Resource
class_name Sound

@export var name: String
@export var file: AudioStreamMP3
@export var volume: float = 0.0
@export var pitch: float = 1.0
@export var autoplay: bool = false

func SetName():
	if file != null:
		name = file.resource_path.split("/")[-1].split(".")[0]

func SetValues(_name: String, _file: AudioStreamMP3):
	name = _name
	file = _file
