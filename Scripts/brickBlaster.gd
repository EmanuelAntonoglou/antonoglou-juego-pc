extends Node2D
var window
var win: bool = false
var lose: bool = false
var currentLevel: int = 0

@onready var CHealth = $"Component Health"
@onready var paddle = $Paddle
@onready var hearts = $UI/Hearts.get_children()
@onready var powerUps = $PowerUps
@onready var ui = $UI
@onready var labelLose = $UI/LabelLose
@onready var levels = $Levels.get_children()
@onready var levelBlocks: int = 0
@onready var balls = $Balls
@onready var paddleSelector = $PaddleSelector
@onready var APMusic = $APMusic
@onready var APWin = $APWin
@onready var APLose = $APLose
@onready var powerUp = $UI/PowerUp
@onready var circle = $UI/PowerUp/Circle
@onready var cooldown = $UI/PowerUp/Cooldown

func _ready():
	paddle.game = self
	$UI.visible = false

func ChangePaddle(color: int):
	Activate(paddle)
	paddle.balls.clear()
	paddle.ChangeColor(color)
	paddle.InstantiateBallPaddle()
	paddle.balls[0].ChangeColor(paddle.currentColor)
	if color == paddle.BrickColor.Red:
		Activate(powerUp)
		powerUp.get_node("PowerUp").texture = preload("res://Images/UI/Power Up Red.png")
		circle.texture = preload("res://Images/UI/Circle Red.png")
	elif color == paddle.BrickColor.Purple:
		Activate(powerUp)
		powerUp.get_node("PowerUp").texture = preload("res://Images/UI/Power Up Purple.png")
		circle.texture = preload("res://Images/UI/Circle Purple.png")
	else:
		Deactivate(powerUp)

func CountBlocks():
	for i in levels[currentLevel-1].get_children():
		var cant = i.get_children()
		levelBlocks += cant.size()

func TakeDamage():
	CHealth.health -= 1
	if CHealth.health >= 0:
		hearts[CHealth.health].TakeDamage()
		if CHealth.health > 0:
			paddle.InstantiateBallPaddle()
		elif CHealth.health == 0:
			Lose()

func RecoverLife():
	if CHealth.health != 5:
		CHealth.health += 1
		hearts[CHealth.health-1].RecoverLife()

func Deactivate(node):
	node.visible = false
	node.process_mode = Node.PROCESS_MODE_DISABLED

func Activate(node):
	node.visible = true
	node.process_mode = Node.PROCESS_MODE_INHERIT

func StartLevel(level):
	currentLevel = level
	$UI/LabelLevel.text = "Level " + str(currentLevel)
	Activate(ui)
	Activate(levels[level-1])
	CountBlocks()
	APMusic.play()

func Win():
	if not lose:
		win = true
		labelLose.text = "You Win!"
		labelLose.visible = true
		paddle.ball_throw = true
		$TimerWin.start()
		APMusic.stop()
		APWin.play()
		powerUp.visible = false
		paddle.get_node("TimerPowerUpCooldown").start(0)

func Lose():
	lose = true
	labelLose.text = "You Lose!"
	labelLose.visible = true
	APLose.play()

func _on_timer_win_timeout():
	win = false
	labelLose.visible = false
	while CHealth.health != 5:
		RecoverLife()
	for ball in paddle.balls:
		paddle.RemoveBall()
	for ball in balls.get_children():
		ball.queue_free()
	paddle.ball_throw = false
	paddle.position = paddle.original_pos
	#paddle.InstantiateBallPaddle()
	#paddle.balls[0].ChangeColor(paddle.currentColor)
	StartLevel(currentLevel+1)
	Deactivate(paddle)
	Activate(paddleSelector)
