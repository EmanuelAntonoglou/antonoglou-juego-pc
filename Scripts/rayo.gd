extends Node2D
class_name Rayo

@export var movement_speed: float = 450.0
@export var disappear_speed: float = 500.0

var extending: bool = true
var stop_extending: bool = false
var disappear: bool = false
var moving: bool = false
var movement: Vector2 = Vector2.ZERO
var pos_stop_ray: Vector2 = Vector2.ZERO

@onready var parent = get_parent() as NaveEnemiga
@onready var rb = $RigidBody2D
@onready var animation = $Sprite2D/AnimationPlayer

func _ready():
	animation.play("Disparo")

func _process(delta):
	if moving:
		move(delta)
	if extending and not stop_extending:
		Extend(delta)
	if disappear:
		Disappear(delta)

func move(delta):
	movement = Vector2.UP * movement_speed * delta
	rb.move_and_collide(movement)
	position = rb.position

func Extend(delta):
	scale.y += 6.2 * delta
	position.x -= 120 * delta

func Disappear(delta):
	extending = false
	stop_extending = true
	global_position.y = pos_stop_ray.y
	global_position.x -= movement_speed * delta

func change_position(pos):
	position = pos
	rb.position = position

func _on_timer_extend_timeout():
	stop_extending = true
	$TimerDuration.start()

func _on_timer_duration_timeout():
	disappear = true
	pos_stop_ray = global_position
