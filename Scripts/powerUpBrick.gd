@tool
extends Node2D
class_name PowerUp

@export var currentColor: BrickColor = BrickColor.Green
enum BrickColor {
	None,
	Blue,
	Green,
	Yellow,
	Orange,
	Red,
	Purple }

@onready var left = $RigidBody2D/Left
@onready var middle = $RigidBody2D/Middle
@onready var right = $RigidBody2D/Right

func _ready():
	left.frame_coords.y = currentColor - 1
	middle.frame_coords.y = currentColor - 1
	right.frame_coords.y = currentColor - 1

func _process(_delta):
	if Engine.is_editor_hint():
		left.frame_coords.y = currentColor - 1
		middle.frame_coords.y = currentColor - 1
		right.frame_coords.y = currentColor - 1

func _on_area_2d_area_entered(area):
	if area.is_in_group("Destroy"):
		queue_free()
