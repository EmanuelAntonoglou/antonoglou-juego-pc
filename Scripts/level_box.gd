@tool
extends Node2D

@export var level: int = 1

@onready var game = get_parent().get_parent()
@onready var label = $Label

func _ready():
	if not Engine.is_editor_hint():
		$Label.text = "Level " + str(level)

func _process(_delta):
	if Engine.is_editor_hint():
		$Label.text = "Level " + str(level)

func Deactivate(node):
	node.visible = false
	node.process_mode = Node.PROCESS_MODE_DISABLED

func Activate(node):
	node.visible = true
	node.process_mode = Node.PROCESS_MODE_INHERIT

func _on_texture_button_button_down():
	var parent = get_parent()
	Deactivate(parent)
	game.StartLevel(level)
	Activate(game.paddleSelector)

func _on_texture_button_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_texture_button_mouse_exited():
	GameManager.ChangeMouse("Arrow")
