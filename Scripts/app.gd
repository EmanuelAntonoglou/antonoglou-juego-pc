extends Node2D

@export var app: CurrentApp
enum CurrentApp {
	MusicPlayer,
	Naves,
	BrickBlaster }
@export var appName: String = ""

var cantidad_clicks: int = 0
static var appIcons = {}
#var appIcons: Array[Texture] = []

var resAppIcons: ResourceGroup = load("res://ResourceGroup/Images/appIcons.tres")
var window_scene: PackedScene = preload("res://Scenes/window.tscn")
var button_scene: PackedScene = preload("res://Scenes/app_button_taskbar.tscn")

@onready var spriteIcon = $SpriteIcon
@onready var labelName = $LabelName

func _ready():
	labelName.text = appName
	for appIcon in resAppIcons.paths:
		var fileName = appIcon.split("/")[-1].split(".")[0]
		appIcons[fileName] = load(appIcon)
	if app == CurrentApp.MusicPlayer:
		for appIcon in appIcons:
			if appIcon == "MusicPlayer":
				spriteIcon.texture = appIcons["Music"]
				spriteIcon.position = $MarkerSprite.position
				spriteIcon.scale *= 2
	elif app == CurrentApp.BrickBlaster:
		for appIcon in appIcons:
			if appIcon == "Brick Blaster":
				spriteIcon.texture = appIcons["Brick Blaster"]
				spriteIcon.position = $MarkerSprite.position + Vector2(10 ,0)
				#spriteIcon.scale *= 2

func _on_timer_timeout():
	cantidad_clicks = 0

func open_app():
	var window = window_scene.instantiate()
	$"../../Windows".add_child(window)
	window.set_app(app)
	AddAppTaskbar(window)

func AddAppTaskbar(window):
	var button = button_scene.instantiate() as WindowsButton
	button.position = GameManager.taskbar.get_node("MarkerNextApp").position
	GameManager.taskbar.add_child(button)
	window.taskbar_button = button
	window.taskbar_button.window = window
	button.app = app
	button.Initialize()

func _on_button_button_down():
	cantidad_clicks += 1
	$TimerDobleClick.start()
	if cantidad_clicks == 2:
		open_app()

func _on_button_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_button_mouse_exited():
	GameManager.ChangeMouse("Arrow")
