@tool
extends Node2D
class_name Brick

@export var currentColor: BrickColor = BrickColor.Blue
enum BrickColor {
	None,
	Blue,
	Green,
	Yellow,
	Orange,
	Red,
	Purple }

@onready var CHealth = $"Component Health" as ComponentHealth
@onready var sprite = $Sprite2D
@onready var animation = $Sprite2D/AnimationPlayer
@onready var APHit = $APHit
@onready var game = get_parent().get_parent().get_parent().get_parent()
var powerUp_scene = preload("res://Scenes/powerUpBrick.tscn")

func _ready():
	CHealth.health = currentColor
	sprite.frame_coords = Vector2(0, currentColor - 1)

func _process(_delta):
	if Engine.is_editor_hint():
		sprite.frame_coords = Vector2(0, currentColor - 1)

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_PREDELETE:
			on_predelete()

func on_predelete() -> void:
	if not Engine.is_editor_hint():
		game.levelBlocks -= 1
		if game.levelBlocks == 0 and not game.window.closed:
			game.Win()
		if game != null and game.powerUps != null:
			if not currentColor == BrickColor.Blue:
				var powerUp_instance = powerUp_scene.instantiate() as PowerUp
				powerUp_instance.position = position + Vector2(0, 90)
				if currentColor == BrickColor.Green:
					powerUp_instance.currentColor = powerUp_instance.BrickColor.Green
				elif currentColor == BrickColor.Yellow:
					powerUp_instance.currentColor = powerUp_instance.BrickColor.Yellow
				elif currentColor == BrickColor.Orange:
					powerUp_instance.currentColor = powerUp_instance.BrickColor.Orange
				elif currentColor == BrickColor.Red:
					powerUp_instance.currentColor = powerUp_instance.BrickColor.Red
				elif currentColor == BrickColor.Purple:
					powerUp_instance.currentColor = powerUp_instance.BrickColor.Purple
				game.powerUps.add_child(powerUp_instance)

func _on_area_2d_area_entered(area):
	if area.is_in_group("Ball"):
		if CHealth.health == 6:
			animation.current_animation = "Purple"
		elif CHealth.health == 5:
			animation.current_animation = "Red"
		elif CHealth.health == 4:
			animation.current_animation = "Orange"
		elif CHealth.health == 3:
			animation.current_animation = "Yellow"
		elif CHealth.health == 2:
			animation.current_animation = "Green"
		CHealth.TakeDamage()
		APHit.play()
