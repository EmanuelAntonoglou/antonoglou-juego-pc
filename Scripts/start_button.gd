extends Node2D

var startMenu
var mouse_on_menu: bool = false

@onready var button = $TextureButton

func _ready():
	startMenu = get_parent().get_node("Start Menu")

func _input(event):
	if event is InputEventMouseButton:
		if Input.is_action_just_released("MB0") and not mouse_on_menu:
			startMenu.visible = false
			button.button_pressed = false

func _on_texture_button_button_down():
	if not button.button_pressed:
		startMenu.visible = true
	else:
		startMenu.visible = false

func _on_texture_button_mouse_entered():
	GameManager.ChangeMouse("Hover")
	mouse_on_menu = true

func _on_texture_button_mouse_exited():
	GameManager.ChangeMouse("Arrow")
	mouse_on_menu = false

func _on_b_reboot_mouse_entered():
	GameManager.ChangeMouse("Hover")
	mouse_on_menu = true

func _on_b_reboot_mouse_exited():
	GameManager.ChangeMouse("Arrow")
	mouse_on_menu = false

func _on_b_shut_down_mouse_entered():
	GameManager.ChangeMouse("Hover")
	mouse_on_menu = true

func _on_b_shut_down_mouse_exited():
	GameManager.ChangeMouse("Arrow")
	mouse_on_menu = false
