extends Node2D
class_name Wind

@export var canFreeze: bool = false

var focused: bool = false

#region App
static var max_id: int = 1
var id: int = 1
var app: CurrentApp
enum CurrentApp {
	MusicPlayer,
	Naves,
	BrickBlaster }
var app_insatance
var ram: float = 0

var taskbar_button
var taskbar_id: int = 0

var juegoNaves_scene: PackedScene = preload("res://Scenes/juego_naves.tscn")
var musicPlayer_scene: PackedScene = preload("res://Scenes/musicPlayer.tscn")
var brickBlaster_scene: PackedScene = preload("res://Scenes/BrickBlaster.tscn")
#endregion
#region Move
var moving: bool = false
var click_position: Vector2 = Vector2.ZERO
#endregion
#region Maximize
var maximized: bool = false
var position_before_maximized: Vector2 = Vector2.ZERO
#endregion

var minimized: bool = false
var closed: bool = false

var game

@onready var labelName = $Label

func _ready():
	id = max_id
	max_id += 1
	center_window()
	GameManager.taskbar.opened_windows.append(self)
	taskbar_id = GameManager.taskbar.taskbar_id
	GameManager.taskbar.taskbar_id += 1
	focus_windows()
	if GameManager.ram > GameManager.maxRam:
		GameManager.crashed = true
		GameManager.startButton.Reboot()

func _physics_process(_delta):
	if moving and Input.is_action_just_released("MB0"):
		moving = false
	move()

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_PREDELETE:
			on_predelete()

func on_predelete() -> void:
	taskbar_button.queue_free()
	if app == CurrentApp.Naves:
		GameManager.taskbar.get_node("MarkerNextApp").position.x -= 210
	elif app == CurrentApp.MusicPlayer:
		GameManager.taskbar.get_node("MarkerNextApp").position.x -= 255
	elif app == CurrentApp.BrickBlaster:
		GameManager.taskbar.get_node("MarkerNextApp").position.x -= 280
	
	for window in range(GameManager.taskbar.opened_windows.size()):
		if window > taskbar_id:
			if app == CurrentApp.Naves:
				GameManager.taskbar.opened_windows[window].taskbar_button.position.x -= 210
			elif app == CurrentApp.MusicPlayer:
				GameManager.taskbar.opened_windows[window].taskbar_button.position.x -= 255
			elif app == CurrentApp.BrickBlaster:
				GameManager.taskbar.opened_windows[window].taskbar_button.position.x -= 280
			GameManager.taskbar.opened_windows[window].taskbar_id -= 1
	GameManager.taskbar.opened_windows.remove_at(taskbar_id)
	GameManager.taskbar.taskbar_id -= 1
	GameManager.ram -= ram

func set_app(_app: CurrentApp):
	app = _app
	open_app()
	if app == CurrentApp.Naves:
		labelName.text = "Invaders.exe"
	elif app == CurrentApp.MusicPlayer:
		labelName.text = "Music Player.exe"
	elif app == CurrentApp.BrickBlaster:
		labelName.text = "Brick Blaster.exe"
	print("Aplicacion: " + str(app) + " ID: " + str(id))

func center_window():
	var screen_size = get_viewport_rect().size / 2.0
	set_position(screen_size)
	focus_windows()

func open_app():
	var image
	var escala
	if app == CurrentApp.Naves:
		app_insatance = juegoNaves_scene.instantiate() as JuegoNaves
		escala = Vector2(0.261, 0.33)
		image = preload("res://Images/Juego Naves/FondoEspacioPrueba.jpg")
		change_background(escala, image)
		canFreeze = true
		ram = 30
	elif app == CurrentApp.MusicPlayer:
		app_insatance = musicPlayer_scene.instantiate()
		ram = 40
		#escala = Vector2(0.261, 0.33)
		#image = preload("res://Images/Juego Naves/FondoEspacioPrueba.jpg")
		#change_background(escala, image)
	elif app == CurrentApp.BrickBlaster:
		app_insatance = brickBlaster_scene.instantiate()
		escala = Vector2(0.261, 0.33)
		image = preload("res://Images/Brick Blaster/FondoBrick.png")
		ram = 30
		change_background(escala, image)
		canFreeze = true
	$FondoApp.add_child(app_insatance)
	app_insatance.window = self
	GameManager.ram += ram

func change_background(escala, texture):
	$FondoApp.texture = texture
	$FondoApp.scale = escala

func move():
	if moving:
		if maximized:
			resize_window(false)
		var mouse_position = get_global_mouse_position()
		var local_mouse_position = to_local(mouse_position)
		position += local_mouse_position - click_position

func focus_windows():
	focused = true
	raise_top()
	FocusTaskbarButton()
	if app_insatance != null:
		game = get_node("FondoApp")
		game.process_mode = Node.PROCESS_MODE_INHERIT

func raise_top():
	var parent = get_parent()
	parent.remove_child(self)
	parent.add_child(self)
	for window in range(GameManager.taskbar.opened_windows.size()):
		game = GameManager.taskbar.opened_windows[window].get_node("FondoApp")
		if window != taskbar_id and GameManager.taskbar.opened_windows[window].canFreeze:
			game.process_mode = Node.PROCESS_MODE_DISABLED
		elif window != taskbar_id and not GameManager.taskbar.opened_windows[window].canFreeze:
			game.process_mode = Node.PROCESS_MODE_INHERIT
	process_mode = Node.PROCESS_MODE_INHERIT

func FocusTaskbarButton():
	for window in range(GameManager.taskbar.opened_windows.size()):
		if window != taskbar_id:
			GameManager.taskbar.opened_windows[window].taskbar_button.texture_button.button_pressed = false
	if taskbar_button != null and not taskbar_button.texture_button.button_pressed:
		taskbar_button.texture_button.button_pressed = false

func resize_window(maximize):
	if maximize:
		maximized = true
		focus_windows()
		position_before_maximized = position
		position = Vector2(564, 509)
		set_scale(Vector2(6.936, 6.385))
	else:
		maximized = false
		if not moving:
			position = position_before_maximized
		set_scale(Vector2(3.5, 3.5))

func Minimize():
	visible = false
	if canFreeze:
		game.process_mode = Node.PROCESS_MODE_DISABLED
	for window in range(GameManager.taskbar.opened_windows.size()):
		taskbar_button.texture_button.button_pressed = false

func Unminimized():
	visible = true
	taskbar_button.texture_button.button_pressed = true

#func _input(event):
	#if event is InputEventMouseButton:
		#if Input.is_action_just_released("MB0") and (not minimized or not closed):
			##game.process_mode = Node.PROCESS_MODE_INHERIT
			#pass

func _on_drag_button_down():
	focus_windows()
	click_position = get_local_mouse_position()
	moving = true
	taskbar_button.texture_button.button_pressed = true
	GameManager.ChangeMouse("Hover")
	#if canFreeze:
		#game.process_mode = Node.PROCESS_MODE_DISABLED

func _on_close_button_down():
	queue_free()
	closed = true

func _on_app_button_down():
	focus_windows()
	taskbar_button.texture_button.button_pressed = true

func _on_maximize_button_down():
	if maximized:
		resize_window(false)
		focus_windows()
	else:
		resize_window(true)
	taskbar_button.texture_button.button_pressed = true
	for window in range(GameManager.taskbar.opened_windows.size()):
		if window != taskbar_id:
			GameManager.taskbar.opened_windows[window].taskbar_button.texture_button.button_pressed = false

func _on_minimize_button_down():
	Minimize()
	minimized = true

func Focus():
	focus_windows()
	taskbar_button.texture_button.button_pressed = true

func _on_drag_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_drag_mouse_exited():
	GameManager.ChangeMouse("Arrow")

func _on_minimize_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_minimize_mouse_exited():
	GameManager.ChangeMouse("Arrow")

func _on_maximize_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_maximize_mouse_exited():
	GameManager.ChangeMouse("Arrow")

func _on_close_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_close_mouse_exited():
	GameManager.ChangeMouse("Arrow")

