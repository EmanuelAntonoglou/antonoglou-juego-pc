extends Sprite2D

@export var time: int = 420

@onready var labelTime = $Label

func SetTimeFormatted():
	@warning_ignore("integer_division")
	var minutes: int = int(time / 60)
	var seconds: int = int(time - (minutes * 60))
	var seconds_str = str(seconds).pad_zeros(2)
	
	if time <= 720:
		@warning_ignore("integer_division")
		labelTime.text = str(minutes).pad_zeros(2) + ":" + seconds_str + " AM"
	elif time < 1380:
		@warning_ignore("integer_division")
		var minutesPM = int((time - 720 + 60) / 60)
		labelTime.text = str(minutesPM).pad_zeros(2) + ":" + seconds_str + " PM"
	elif time >= 1440:
		time = 0
		@warning_ignore("integer_division")
		if minutes == 24:
			minutes = 00
		labelTime.text = str(minutes).pad_zeros(2) + ":" + seconds_str + " AM"

func _on_timer_timeout():
	time += 18
	SetTimeFormatted()
