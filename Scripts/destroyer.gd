extends Node2D

@export var groups: Array[String]

@onready var hitbox = $Area2D

func _process(_delta):
	Detect_Collision()

func Detect_Collision():
	var areas = hitbox.get_overlapping_areas()
	for area in areas:
		for group in groups:
			if area.is_in_group(group):
				area.get_parent().queue_free()
				return
