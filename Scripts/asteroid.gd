extends Node2D
class_name Asteroid

@export var groups: Array[String]

@onready var parent = get_parent() as JuegoNaves
@onready var rb = $RigidBody2D
@onready var nave = parent.get_node("Nave")
@onready var hitbox = $Area2D
@onready var animation = $Sprite2D/AnimationPlayer
@onready var CHealth = $"Component Health"
@onready var CMove = $"Component Move"

func _ready():
	rb.position = position

func _process(_delta):
	if hitbox.monitoring:
		Detect_Collision()

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_PREDELETE:
			on_predelete()

func on_predelete() -> void:
	if CHealth.killed_by_player:
		Instantiate_Shield()

func Instantiate_Shield():
	if nave != null and not nave.has_shield:
		nave.has_shield = true
		var shield = nave.shield_scene.instantiate()
		shield.position = nave.markerShield.position
		nave.add_child(shield)

func Detect_Collision():
	var areas = hitbox.get_overlapping_areas()
	for area in areas:
		for group in groups:
			if area.is_in_group(group):
				area.get_parent().queue_free()
				CHealth.TakeDamage()
				if CHealth.health == 0:
					get_parent().PlayAsteroidDeath()
				return
