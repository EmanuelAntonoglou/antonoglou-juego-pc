extends Node2D

var window
static var instancesOpen: int = 0
@onready var currentSong: int = 0
var audioPosition: float = 0
var audiosMusicPlayer: Array[Sound] = []
var volumeBeforeMute: float = 0
var sliderPosDragStarted: bool = false

@onready var audioPlayer = $AudioPlayer
@onready var TBPlay = $TBPlay
@onready var LabelSongName = $LabelSongName
@onready var sliderVolume = $SliderVolume
@onready var sliderPosition = $SliderPosition
@onready var labelPosition = $LabelPosition
@onready var labelDuration = $LabelDuration
@onready var TBSound = $TBSound

var resMusicPlayer: ResourceGroup = load("res://ResourceGroup/Audio/musicPlayer.tres")

func _ready():
	instancesOpen += 1
	currentSong = AudioManager.musicPlayerSong
	AppendToAudioList()
	var song = audiosMusicPlayer[currentSong].file
	audioPlayer.stream = song
	LabelSongName.text = "Track " + str(currentSong + 1) + " - " + audiosMusicPlayer[currentSong].name
	audioPlayer.volume_db = AudioManager.musicPlayerVolume
	sliderVolume.value = audioPlayer.volume_db
	audioPosition = AudioManager.musicPlayerPosition
	sliderPosition.value = AudioManager.musicPlayerPosition
	GetPosition()
	GetSongDuration()
	Play(AudioManager.musicPlayerPosition)
	Pause()

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_PREDELETE:
			on_predelete()

func on_predelete() -> void:
	instancesOpen -= 1
	if instancesOpen == 0:
		AudioManager.musicPlayerPosition = audioPosition
		AudioManager.musicPlayerVolume = audioPlayer.volume_db
		AudioManager.musicPlayerSong = currentSong

func AppendToAudioList():
	for audioPath in resMusicPlayer.paths:
		var fileName = audioPath.split("/")[-1].split(".")[0]
		var newSound = Sound.new()
		newSound.SetValues(fileName, load(audioPath))
		audiosMusicPlayer.append(newSound)

func Play(pos: float = 0):
	if pos > 0.0:
		audioPlayer.play(pos)
	else:
		audioPlayer.play()
	LabelSongName.text = "Track " + str(currentSong + 1) + " - " + audiosMusicPlayer[currentSong].name
	GetPosition()
	GetSongDuration()
	sliderPosition.max_value = audioPlayer.stream.get_length()

func Pause():
	audioPlayer.stop()

func GetPosition():
	SetTimeFormatted(labelPosition, audioPosition)

func GetSongDuration():
	var duration_seconds = audioPlayer.stream.get_length()
	SetTimeFormatted(labelDuration, duration_seconds)

func SetTimeFormatted(label: Label, time: float):
	var minutes = int(time / 60)
	var seconds = int(time - (minutes * 60))
	var seconds_str = str(seconds).pad_zeros(2)
	label.text = str(minutes) + ":" + seconds_str

func SetSongAndPlay():
	audioPosition = 0
	var song = audiosMusicPlayer[currentSong].file
	audioPlayer.stream = song
	Play(audioPosition)
	TBPlay.button_pressed = true

func SetNextSong():
	if (currentSong + 1) != audiosMusicPlayer.size():
		currentSong += 1
	else:
		currentSong = 0
	if instancesOpen == 0:
		AudioManager.musicPlayerSong = currentSong
	SetSongAndPlay()

func _on_tb_play_button_down():
	window.Focus()
	GetPosition()
	if not TBPlay.button_pressed:
		Play(audioPosition)
	else:
		Pause()
	TBPlay.button_pressed = not TBPlay.button_pressed

func _on_tb_next_button_down():
	SetNextSong()
	window.Focus()

func _on_tb_back_button_down():
	if (currentSong - 1) < 0:
		currentSong = (audiosMusicPlayer.size() - 1)
	else:
		currentSong -= 1
	if instancesOpen == 0:
		AudioManager.musicPlayerSong = currentSong
	SetSongAndPlay()
	window.Focus()

func _on_slider_volume_drag_ended(_value_changed):
	sliderVolume.release_focus()

func _on_slider_volume_value_changed(_value):
		if sliderVolume.value == -20:
			TBSound.button_pressed = true
			audioPlayer.volume_db = -80
		elif sliderVolume.value > -20:
			TBSound.button_pressed = false
			audioPlayer.volume_db = sliderVolume.value

func _on_audio_player_finished():
	SetNextSong()

func _on_timer_check_song_pos_timeout():
	if not sliderPosDragStarted and TBPlay.button_pressed:
		audioPosition = audioPlayer.get_playback_position()
		sliderPosition.value = audioPosition
		GetPosition()

func _on_slider_position_drag_started():
	sliderPosDragStarted = true

func _on_slider_position_drag_ended(_value_changed):
	audioPosition = sliderPosition.value
	GetPosition()
	sliderPosDragStarted = false
	if TBPlay.button_pressed:
		Play(audioPosition)
	if audioPosition >= audioPlayer.stream.get_length():
		SetNextSong()

func _on_tb_sound_button_down():
	if sliderVolume.value > -20:
		volumeBeforeMute = audioPlayer.volume_db
		sliderVolume.value = -20
	elif sliderVolume.value == -20:
		sliderVolume.value = volumeBeforeMute
	window.Focus()

func _on_slider_volume_drag_started():
	volumeBeforeMute = audioPlayer.volume_db
