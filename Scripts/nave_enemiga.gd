extends Node2D
class_name NaveEnemiga

@export var is_kamikase: bool = false
@export var is_hangar: bool = false

@onready var parent = get_parent() as JuegoNaves
@onready var rb = $RigidBody2D
@onready var hitbox = $Area2D
@onready var CHealth = $"Component Health"
@onready var CShoot
@onready var CMove = $"Component Move"
@onready var APShoot = $APShoot
@onready var APDeath = $APDeath

func _ready():
	if self.has_node("Component Shoot"):
		CShoot = $"Component Shoot"
	parent.enemies_left += 1

func _process(_delta):
	if hitbox.monitoring:
		Detect_Collision()

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_PREDELETE:
			on_predelete()

func on_predelete() -> void:
	parent.enemies_left -= 1

func Detect_Collision():
	var areas = hitbox.get_overlapping_areas()
	for area in areas:
		if area.is_in_group("BalaPersonaje"):
			area.get_parent().queue_free()
			CHealth.TakeDamage()
			if CHealth.health == 0:
				APDeath.play()
		if area.is_in_group("Player") and is_kamikase:
			CHealth.Die()
			CMove.move_to_target = false
			return

func change_position(pos):
	position = pos
	rb.position = position

func _on_animation_player_animation_finished(_anim_name):
	if is_hangar and CHealth.health != 0:
		$Sprite2D/AnimationPlayer.play()
		CShoot.Shoot()
