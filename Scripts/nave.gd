extends Node2D
class_name Nave

@export var groups: Array[String]

var vel: float = 155
var movement: Vector2 = Vector2.ZERO
var has_shield: bool = false
var shield_scene: PackedScene = preload("res://Scenes/shield.tscn")

@onready var parent = get_parent() as JuegoNaves
@onready var rb = $RigidBody2D
@onready var hitbox = $Area2D
@onready var markerShield = $MarkerShield
@onready var CHealth = $"Component Health"
@onready var CShoot = $"Component Shoot"
@onready var APHit = $APHit
@onready var APShieldBroken = $APShieldBroken

func _ready():
	rb.position = position

func _process(delta):
	if hitbox.monitoring:
		Detect_Collision()
	Move(delta)
	if CShoot.can_shoot and Input.is_action_pressed("Disparo"):
		CShoot.Shoot()

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_PREDELETE:
			on_predelete()

func on_predelete() -> void:
	if parent != null and not parent.closed and not parent.won:
		if parent.has_node("LabelWinLose"):
			parent.get_node("LabelWinLose").text = "YOU LOSE!"
			parent.get_node("LabelWinLose").visible = true
			get_parent().get_node("APLose").play()

func Detect_Collision():
	var areas = hitbox.get_overlapping_areas()
	if not has_shield and CHealth.is_vulnerable:
		for area in areas:
			for group in groups:
				if area.is_in_group(group):
					CHealth.TakeDamage()
					APHit.play()
					if area.is_in_group("EnemyBullet"):
						area.get_parent().queue_free()
					return
	elif has_shield:
		for area in areas:
			for group in groups:
				if area.is_in_group(group):
					if area.is_in_group("Asteroid"):
						area.get_parent().queue_free()
						has_shield = false
					return

func Move(delta):
	var direction = Input.get_vector("left", "right", "up", "down")
	if direction != Vector2.ZERO:
		movement = direction * vel * delta
		rb.move_and_collide(movement)
		position = rb.position
