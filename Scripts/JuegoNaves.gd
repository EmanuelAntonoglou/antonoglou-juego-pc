extends Node2D
class_name JuegoNaves

@export var wave: int = 0
@export var enemies_round1: Array[EnemyTypes]
@export var enemies_round2: Array[EnemyTypes]
@export var enemies_round3: Array[EnemyTypes]
@export var enemies_roundBoss: Array[EnemyTypes]

var window
var enemies_rounds: Array = []
var enemyTypes: EnemyTypes
enum EnemyTypes {
	EnemyBullet,
	EnemyRay,
	EnemyHangar,
	Boss,
	EnemyAsteroid }
var enemies_left: int = 0
var won: bool = false
var closed: bool = false
var volume_before_mute: float = 0

var enemyBullet_scene: PackedScene = preload("res://Scenes/nave_enemiga.tscn")
var enemyRay_scene: PackedScene = preload("res://Scenes/nave_enemiga_2.tscn")
var enemyHangar_scene: PackedScene = preload("res://Scenes/enemy_hangar.tscn")
var enemyAsteroid_scene: PackedScene = preload("res://Scenes/asteroid.tscn")
var boss_scene: PackedScene = preload("res://Scenes/ship_boss.tscn")

@onready var labelWave = $LabelWave
@onready var labelWinLose = $LabelWinLose
@onready var APMusic = $APMusic
@onready var APWin = $APWin
@onready var APLose = $APLose

func _ready():
	labelWave.label_settings.font_color = Color.from_string("ffe1e6", Color.WHITE)
	enemies_rounds.append(enemies_round1)
	enemies_rounds.append(enemies_round2)
	enemies_rounds.append(enemies_round3)
	enemies_rounds.append(enemies_roundBoss)
	APMusic.play()

func _process(_delta):
	wave_controler()

func wave_controler():
	if wave != (enemies_rounds.size() + 1) and enemies_left == 0:
		wave += 1
		if wave != (enemies_rounds.size() + 1):
			Start_wave()
		else:
			GameManager.won_spaceship = true
			won = true
			labelWinLose.visible = true
			labelWave.visible = false
			APMusic.stop()
			APWin.play()

func Start_wave():
	for enemy in range(enemies_rounds[wave-1].size()):
		if wave != enemies_rounds.size():
			labelWave.text = "WAVE " + str(wave)
			SpawnEnemy(enemy)
			SpawnAsteroid(enemy)
		else: #if boss stage
			labelWave.text = "BOSS"
			labelWave.label_settings.font_color = Color.from_string("f5006f", Color.WHITE)
			SpawnBoss()

func SpawnEnemy(enemy):
	var enemyShip
	var spawn_enemy_list = $"Spawns Enemy".get_children().duplicate()
	spawn_enemy_list.shuffle()
	
	if enemies_rounds[wave-1][enemy] == EnemyTypes.EnemyBullet:
		enemyShip = enemyBullet_scene.instantiate() as NaveEnemiga
	elif enemies_rounds[wave-1][enemy] == EnemyTypes.EnemyRay:
		enemyShip = enemyRay_scene.instantiate() as NaveEnemiga
	elif enemies_rounds[wave-1][enemy] == EnemyTypes.EnemyHangar:
		enemyShip = enemyHangar_scene.instantiate() as NaveEnemiga
	var spawn_index = enemy % randi() % spawn_enemy_list.size()
	
	if enemyShip != null:
		$".".add_child(enemyShip)
		enemyShip.CMove.change_position(spawn_enemy_list[spawn_index].position)

func SpawnAsteroid(enemy):
	if enemies_rounds[wave-1][enemy] == EnemyTypes.EnemyAsteroid:
		var asteroid = enemyAsteroid_scene.instantiate() as Asteroid
		var final_spawn_asteroid_list = []
		var spawn_asteroid_list = $"Spawns Asteroid".get_children(true).duplicate()
		for child in spawn_asteroid_list:
			var spawn_asteroid_up_down = child.get_children()
			spawn_asteroid_up_down.shuffle()
			final_spawn_asteroid_list.append(spawn_asteroid_up_down)
		spawn_asteroid_list = final_spawn_asteroid_list
		var random_0_1 = int(floor(randf() * 2.0))
		var random_spawn_asteoid = randi() % spawn_asteroid_list[random_0_1].size()
		
		if asteroid != null:
			$".".add_child(asteroid)
			asteroid.CMove.change_position(spawn_asteroid_list[random_0_1][random_spawn_asteoid].position)
			if random_0_1 == 1:
				asteroid.CMove.ChangeDirection(asteroid.CMove.Direction.UP)

func SpawnBoss():
	var boss = boss_scene.instantiate() as NaveEnemiga
	
	$".".add_child(boss)
	boss.CMove.change_position($"Spawns Enemy/SpawnPosition1".position)
	call_deferred("SpawnAsteroidBoss")

func SpawnAsteroidBoss():
	var asteroid
	var spawn_asteroid_boss_list = $"Spawns Asteroid Boss".get_children().duplicate() 
	
	while not GameManager.won_spaceship:
		if GameManager.won_spaceship:
			return
		await get_tree().create_timer(5).timeout
		asteroid = enemyAsteroid_scene.instantiate() as Asteroid
		$".".add_child(asteroid)
		var random_spawn_asteoid_boss = randi() % spawn_asteroid_boss_list.size()
		asteroid.CMove.change_position(spawn_asteroid_boss_list[random_spawn_asteoid_boss].position)
		asteroid.CMove.ChangeDirection(asteroid.CMove.Direction.LEFT)
		asteroid.CMove.speed = 365

func PlayAsteroidDeath():
	$APDeathAsteroid.play()

func Mute():
	volume_before_mute = APMusic.volume_db
	APMusic.volume_db = -80

func Unmute():
	APMusic.volume_db = -volume_before_mute
