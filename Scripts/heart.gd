extends Node2D

@onready var animation = $Sprite2D/AnimationPlayer

func TakeDamage():
	animation.play("TakeDamage")

func RecoverLife():
	animation.play("RecoverLife")
