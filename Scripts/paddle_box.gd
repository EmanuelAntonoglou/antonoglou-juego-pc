@tool
extends Node2D

@export var currentColor: BrickColor = BrickColor.Blue
enum BrickColor {
	None,
	Blue,
	Green,
	Yellow,
	Orange,
	Red,
	Purple }

@onready var game = get_parent().get_parent()
@onready var TB = $TextureButton
@onready var Borde = $Borde
var image

func _ready():
	if not Engine.is_editor_hint():
		ChangeColor()

func _process(_delta):
	if Engine.is_editor_hint():
		ChangeColor()

func ChangeColor():
	image = $Sprite2D
	if currentColor == BrickColor.Blue:
		TB.self_modulate = Color.from_string("a477bd", Color.WHITE)
		Borde.self_modulate = Color.from_string("654b73", Color.WHITE)
		image.texture = preload("res://Images/UI/Paddle Blue.png")
	elif  currentColor == BrickColor.Green:
		TB.self_modulate = Color.from_string("8a997f", Color.WHITE)
		Borde.self_modulate = Color.from_string("4a5244", Color.WHITE)
		image.texture = preload("res://Images/UI/Paddle Green.png")
	elif currentColor == BrickColor.Yellow:
		TB.self_modulate = Color.from_string("ba8a59", Color.WHITE)
		Borde.self_modulate = Color.from_string("80503d", Color.WHITE)
		image.texture = preload("res://Images/UI/Paddle Yellow.png")
	elif currentColor == BrickColor.Orange:
		TB.self_modulate = Color.from_string("ba7259", Color.WHITE)
		Borde.self_modulate = Color.from_string("a04e47", Color.WHITE)
		image.texture = preload("res://Images/UI/Paddle Orange.png")
	elif currentColor == BrickColor.Red:
		TB.self_modulate = Color.from_string("ba5959", Color.WHITE)
		Borde.self_modulate = Color.from_string("802f49", Color.WHITE)
		image.texture = preload("res://Images/UI/Paddle Red.png")
	elif currentColor == BrickColor.Purple:
		TB.self_modulate = Color.from_string("ba70b4", Color.WHITE)
		Borde.self_modulate = Color.from_string("633359", Color.WHITE)
		image.texture = preload("res://Images/UI/Paddle Purple.png")

func Deactivate(node):
	node.visible = false
	node.process_mode = Node.PROCESS_MODE_DISABLED

func Activate(node):
	node.visible = true
	node.process_mode = Node.PROCESS_MODE_INHERIT

func _on_texture_button_button_down():
	var parent = get_parent()
	Deactivate(parent)
	game.ChangePaddle(currentColor)
	Activate(game.paddle)

func _on_texture_button_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_texture_button_mouse_exited():
	GameManager.ChangeMouse("Arrow")
