extends Node

@onready var boot =  $"../../Boot" as Boot
@onready var startButton =  $"../Start Button"
@onready var about =  $"../../About"

func _ready():
	GameManager.startButton = self

func _on_b_shut_down_button_up():
	self.visible = false
	for window in GameManager.taskbar.opened_windows:
		window.queue_free()
	get_tree().quit()

func _on_b_reboot_button_up():
	GameManager.crashed = false
	Reboot()
	$"../../About".visible = false
	about.process_mode = Node.PROCESS_MODE_DISABLED

func Reboot():
	if GameManager.crashed:
		GameManager.startButton.boot.video_player.stream = load("res://Video/Boot Glitch.ogv")
	else:
		GameManager.startButton.boot.video_player.stream = load("res://Video/Boot Normal.ogv")
	self.visible = false
	startButton.button.button_pressed = false
	boot.login.visible = false
	$"../Time".visible = false
	boot.visible = true
	boot.video_player.play()
	for window in GameManager.taskbar.opened_windows:
		window.queue_free()

func _on_b_about_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_b_about_mouse_exited():
	GameManager.ChangeMouse("Arrow")

func _on_b_reboot_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_b_about_button_up():
	about.visible = true
	about.process_mode = Node.PROCESS_MODE_INHERIT

func _on_close_button_down():
	about.visible = false
	about.process_mode = Node.PROCESS_MODE_DISABLED

func _on_close_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_close_mouse_exited():
	GameManager.ChangeMouse("Arrow")

func _on_cat_button_down():
	AudioManager.ChangeVolume("Meow", 33)
	AudioManager.Play("Meow")

func _on_cat_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_cat_mouse_exited():
	GameManager.ChangeMouse("Arrow")
