extends Node2D
class_name Boot

@export var username = "Admin"
@export var password = ""

var hint = "password!"
var video_ended: bool = false
var pass_line_focussed: bool = false

@onready var video_player = $VideoStreamPlayer
@onready var login = $Login
@onready var pass_line_edit = $Login/LineEditPassword
@onready var username_label = $Login/LabelUsername
@onready var hint_label = $Login/LabelHint


func _ready():
	username_label.text = username
	pass_line_edit.secret = true
	hint_label.text =  "Hint: " + hint
	video_player.stream = load("res://Video/Boot Normal.ogv")
	video_player.play()

func _input(event):
	if event is InputEventMouseButton:
		if Input.is_action_just_pressed("MB0"):
			if not pass_line_focussed:
				pass_line_edit.focus_mode = false
			else:
				pass_line_edit.focus_mode = true

func _process(_delta):
	if not video_player.is_playing():
		video_ended = true
		login.visible = true

func CheckPassword(submitted_pass):
	AudioManager.Play("Typing Enter")
	if submitted_pass == password:
		visible = false
		hint_label.visible = false
		$"../Taskbar/Time".visible = true
	elif submitted_pass != "":
		hint_label.visible = true
	pass_line_edit.text = ""

func _on_line_edit_password_text_submitted(new_text):
	CheckPassword(new_text)

func _on_line_edit_password_text_changed(_new_text):
	AudioManager.PlayRandomTypingSound()

func _on_line_edit_password_mouse_entered():
	GameManager.ChangeMouse("Text")
	pass_line_focussed = true

func _on_line_edit_password_mouse_exited():
	GameManager.ChangeMouse("Arrow")
	pass_line_focussed = false
