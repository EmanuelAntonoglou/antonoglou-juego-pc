extends Node2D
class_name WindowsButton

@export var app: CurrentApp

enum CurrentApp {
	MusicPlayer,
	Naves,
	BrickBlaster }
var app_name = ""
var window
var app_texture

@onready var texture_button = $TextureButton
@onready var label = $RichTextLabel
@onready var app_icon = $"App Icon"

var texture_invaders = preload("res://Images/Window/Invaders Taskbar Icon.png")
var texture_MP = preload("res://Images/App/Music.png")
var texture_BB = preload("res://Images/App/Brick Blaster.png")

func Initialize():
	label.label_settings.font_size = 16
	if app == CurrentApp.Naves:
		GameManager.taskbar.get_node("MarkerNextApp").position.x += 210
		label.text = "Invaders"
		texture_button.scale.x += 0.13
		app_icon.texture = texture_invaders
		label.position.x += 42
	elif app == CurrentApp.MusicPlayer:
		GameManager.taskbar.get_node("MarkerNextApp").position.x += 255
		label.text = "Music Player"
		texture_button.scale.x += 0.26
		#app_icon.scale *= 0.1
		app_icon.texture = texture_MP
		label.position.x += 46
	elif app == CurrentApp.BrickBlaster:
		GameManager.taskbar.get_node("MarkerNextApp").position.x += 280
		label.text = "Brick Blaster"
		texture_button.scale.x += 0.33
		app_icon.scale *= 0.35
		app_icon.texture = texture_BB
		app_icon.position.x += 4
		label.position.x += 54

func _on_texture_button_button_down():
	if window.minimized:
		window.minimized = false
		window.Unminimized()
		texture_button.button_pressed = false
	else:
		if texture_button.button_pressed:
			window.minimized = true
			window.Minimize()
			texture_button.button_pressed = true
	window.focus_windows()

func _on_texture_button_mouse_entered():
	GameManager.ChangeMouse("Hover")

func _on_texture_button_mouse_exited():
	GameManager.ChangeMouse("Arrow")
