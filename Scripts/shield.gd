extends Node2D
class_name Shield

@export var groups: Array[String]

@onready var parent = get_parent() as Nave
@onready var animationL = $Sprite2DL/AnimationPlayer
@onready var animationR = $Sprite2DR/AnimationPlayer
@onready var hitbox = $Area2D

func _ready():
	animationL.play("Shield")
	animationR.play("Shield")

func _process(_delta):
	Detect_Collision()

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_PREDELETE:
			on_predelete()

func on_predelete() -> void:
	get_parent().get_node("APShieldBroken").play()

func Detect_Collision():
	var areas = hitbox.get_overlapping_areas()
	for area in areas:
		for group in groups:
			if area.is_in_group(group):
				parent.CHealth.timerInvulnerable.start()
				parent.CHealth.is_vulnerable = false
				parent.has_shield = false
				if area.is_in_group("EnemyBullet"):
					area.get_parent().queue_free()
				queue_free()
				return
