extends RigidBody2D
class_name Paddle

@export var currentColor: BrickColor = BrickColor.Blue
enum BrickColor {
	None,
	Blue,
	Green,
	Yellow,
	Orange,
	Red,
	Purple }

var speed = 400
var vel = Vector2()
var game
var balls: Array = []
var ball_throw: bool = false
var original_pos: Vector2 = Vector2.ZERO
var purple_pos: Vector2 = Vector2.ZERO
var paddle_offset: Vector2 = Vector2.ZERO
var first_ball: bool = true

var can_paddle_red: bool = false
var can_paddle_purple: bool = false
var paddle_red: bool = false
var paddle_purple: bool = false

@onready var sprite = $Sprite2D
@onready var marker = $Marker2D
@onready var timerPowerUp = $TimerPowerUpDuration
var ball_scene: PackedScene = preload("res://Scenes/ball.tscn")

func _ready():
	original_pos = position
	purple_pos = position

func _physics_process(delta):
	if not game.lose:
		DetectMovement(delta)
		DetectBallThrow()
		DetectPowerUp()
	if game.get_node("UI").get_node("PowerUp").get_node("Cooldown").visible:
		game.get_node("UI").get_node("PowerUp").get_node("Cooldown").text = str(round($TimerPowerUpCooldown.time_left))

func DetectMovement(delta):
	vel = Vector2()
	if Input.is_action_pressed("right"):
		vel.x += 1
	if Input.is_action_pressed("left"):
		vel.x -= 1
	if paddle_purple:
		if Input.is_action_pressed("down"):
			vel.y += 1
			purple_pos.y = position.y
		if Input.is_action_pressed("up"):
			vel.y -= 1
			purple_pos.y = position.y
	position.y = purple_pos.y
	vel = vel.normalized() * speed
	move_and_collide(vel * delta)

func DetectBallThrow():
	if not ball_throw and Input.is_action_just_pressed("spacebar"):
		ball_throw = true
		if balls.size() != 0:
			if first_ball:
				first_ball = false
				balls[0].ChangeColor(currentColor)
			balls[0].StartBall()
			MoveBallToGame()

func DetectPowerUp():
	if can_paddle_red or can_paddle_purple:
		if Input.is_action_just_pressed("Q"):
			if currentColor == BrickColor.Red:
				can_paddle_red = false
				$TimerPowerUpCooldown.wait_time = 10
				speed = 250
				ChangeScaleXY(3.25, 1.3)
				timerPowerUp.start()
			elif currentColor == BrickColor.Purple:
				paddle_purple = true
				$TimerPowerUpCooldown.wait_time = 7
			$TimerPowerUpDuration.start()

func ChangeColor(color: BrickColor):
	currentColor = color
	sprite.frame_coords = Vector2(0, currentColor - 1)
	if color == BrickColor.Yellow:
		speed = 550
		ChangeScale(0.8)
	elif color == BrickColor.Orange:
		speed = 300
		ChangeScale(1.5)
	elif color == BrickColor.Red:
		can_paddle_red = true
	elif color == BrickColor.Purple:
		can_paddle_purple = true

func ChangeScale(new_scale: float):
	sprite.scale.x = new_scale
	$CollisionShape2D.scale.x = new_scale
	$Area2D/CollisionShape2D.scale.x = new_scale

func ChangeScaleXY(new_scale_x: float, new_scale_y: float):
	var new_scale = Vector2(new_scale_x, new_scale_y)
	sprite.scale = new_scale
	$CollisionShape2D.scale = new_scale
	$Area2D/CollisionShape2D.scale = new_scale

func MoveBallToGame():
	remove_child(balls[balls.size()-1])
	balls[balls.size()-1].position = self.position + marker.position
	game.balls.add_child(balls[balls.size()-1])

func InstantiateBallPaddle():
	var ball_instance = ball_scene.instantiate() as Ball
	add_child(ball_instance)
	if currentColor == BrickColor.Orange and first_ball:
		paddle_offset = Vector2(0, -8)
	else:
		paddle_offset = Vector2.ZERO
	ball_instance.global_position = marker.global_position + paddle_offset
	ball_instance.game = game
	balls.append(ball_instance)

func InstantiateBall():
	var ball_instance = ball_scene.instantiate() as Ball
	game.add_child(ball_instance)
	ball_instance.position = self.position + marker.position + Vector2(0, 10)
	balls.append(ball_instance)

func RemoveBall():
	balls.pop_at(0)
	if balls.size() == 0:
		ball_throw = false

func _on_area_2d_area_entered(area):
	if area.is_in_group("Ball"):
		var ball = area.get_parent().get_parent()
		if ball.currentColor == ball.BrickColor.Yellow:
			ball.SpeedUp(20)
		elif ball.currentColor == ball.BrickColor.Orange:
			ball.TryToDivide()
			ball.SpeedUp(20)
		elif ball.currentColor == ball.BrickColor.Red:
			ball.TryToRecoverLife()
		elif ball.currentColor == ball.BrickColor.Purple:
			ball.SpeedUp(20)
	if area.is_in_group("PowerUp"):
		var powerUp = area.get_parent().get_parent()
		powerUp.queue_free()
		if not ball_throw and not game.lose:
			balls[balls.size()-1].queue_free()
			RemoveBall()
		InstantiateBallPaddle()
		balls[balls.size()-1].StartBall()
		balls[balls.size()-1].ChangeColor(powerUp.currentColor as int)
		MoveBallToGame()

func _on_timer_power_up_duration_timeout():
	if currentColor == BrickColor.Red:
		paddle_red = true
		can_paddle_red = false
		speed = 400
		ChangeScaleXY(1, 1)
	elif currentColor == BrickColor.Purple:
		paddle_purple = false
		can_paddle_purple = false
		#Si quiero que vuelva a la posicion original
		#purple_pos.y = original_pos.y
		#position.y = original_pos.y
	$TimerPowerUpCooldown.start()
	game.get_node("UI").get_node("PowerUp").get_node("Cooldown").visible = true
	game.powerUp.get_node("PowerUp").self_modulate = Color.from_string("999999", Color.WHITE)

func _on_timer_power_up_cooldown_timeout():
	if currentColor == BrickColor.Red:
		can_paddle_red = true
		paddle_red = false
	elif currentColor == BrickColor.Purple:
		can_paddle_purple = true
	game.get_node("UI").get_node("PowerUp").get_node("Cooldown").visible = false
	game.powerUp.get_node("PowerUp").self_modulate = Color.from_string("ffffff", Color.WHITE)
