extends Node2D
class_name Bala

@export var movement_speed: int = 450
@export var increase_scale: bool = false
@export var scale_factor: float = 1.003

var moving: bool = true
var movement: Vector2 = Vector2.ZERO

@onready var rb = $RigidBody2D
@onready var animation = $Sprite2D/AnimationPlayer
@onready var APShoot = $APShoot

func _ready():
	animation.play("Disparo")

func _process(delta):
	move(delta)
	if increase_scale:
		IncreaseScale(delta)

func move(delta):
	if moving:
		movement = Vector2.DOWN * movement_speed * delta
		rb.move_and_collide(movement)
		position = rb.position

func change_position(pos):
	position = pos
	rb.position = position

func IncreaseScale(_delta):
	scale *= scale_factor

