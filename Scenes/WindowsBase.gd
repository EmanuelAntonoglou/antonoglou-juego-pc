extends Control

var start : Vector2
var initialPosition : Vector2
var isMoving : bool
var isResizing : bool
var resizeX : bool
var resizeY : bool
var initialSize : Vector2
@export var GrabThreshold: = 20
@export var ResizeThreshold: = 5

func _input(event):
	if Input.is_action_just_pressed("MB0"):
		start_action(event)
	elif Input.is_action_pressed("MB0"):
		if isMoving:
			set_position(initialPosition + (event.position - start))
		elif isResizing:
			resize_windows(event)
	elif Input.is_action_just_released("MB0"):
		stop_actions()

func start_action(event):
	var rect = get_global_rect()
	var localMousePos = event.position - get_global_position()
	if localMousePos.y < GrabThreshold:
		start_move(event)
	else:
		start_resize(event, rect, localMousePos)

func start_move(event):
	start = event.position
	initialPosition = get_global_position()
	isMoving = true

func start_resize(event, rect, localMousePos):
	if abs(localMousePos.x - rect.size.x) < ResizeThreshold:
		start.x = event.position.x
		initialSize.x = get_size().x
		resizeX = true
		isResizing = true
			
	if abs(localMousePos.y - rect.size.y) < ResizeThreshold:
		start.y = event.position.y
		initialSize.y = get_size().y
		resizeY = true
		isResizing = true
			
	if localMousePos.x < ResizeThreshold &&  localMousePos.x > -ResizeThreshold:
		start.x = event.position.x
		initialPosition.x = get_global_position().x
		initialSize.x = get_size().x
		isResizing = true
		resizeX = true
				
	if localMousePos.y < ResizeThreshold &&  localMousePos.y > -ResizeThreshold:
		start.y = event.position.y
		initialPosition.y = get_global_position().y
		initialSize.y = get_size().y
		isResizing = true
		resizeY = true

func resize_windows(event):
	var newWidith = get_size().x
	var newHeight = get_size().y
	
	if resizeX:
		newWidith = initialSize.x - (start.x - event.position.x)
	if resizeY:
		newHeight = initialSize.y - (start.y - event.position.y)
		
	if initialPosition.x != 0:
		newWidith = initialSize.x + (start.x - event.position.x)
		set_position(Vector2(initialPosition.x - (newWidith - initialSize.x), get_position().y))
	
	if initialPosition.y != 0:
		newHeight = initialSize.y + (start.y - event.position.y)
		set_position(Vector2(get_position().x, initialPosition.y - (newHeight - initialSize.y)))
	
	set_size(Vector2(newWidith, newHeight))

func stop_actions():
	isMoving = false
	initialPosition = Vector2(0,0)
	resizeX = false
	resizeY = false
	isResizing = false
