extends Node2D
class_name ComponentShoot

@export var shoot_on_timer: bool = true
@export var bulletType: BulletTypes = BulletTypes.Bullet
@export var has_animation: bool = false
@export var follow_parent: bool = false
@export var timer_shoot: float = 0.75
@export var bullet_speed: float = 450.0
@export var bullet_scene: PackedScene

var can_shoot: bool = false
var start_shooting: bool = false
enum BulletTypes {
	Bullet,
	Ray,
	Enemy }

@onready var parent = get_parent()
@onready var timerShoot = $TimerShoot
@onready var timerShootRay = $TimerShootRay
@onready var APShootRay = $APShootRay
var shoot: bool = false

func _ready():
	timerShoot.wait_time = timer_shoot

func _process(_delta):
	if bulletType != BulletTypes.Ray and start_shooting and can_shoot and shoot_on_timer:
		if has_animation:
			var animation = parent.get_node("Sprite2D/AnimationPlayer")
			animation.play("Shoot")
		Shoot()
	if timerShootRay.time_left < 0.5 and shoot and shoot_on_timer:
		APShootRay.play()
		shoot = false

func Shoot():
	timerShoot.start()
	can_shoot = false
	var bullet = bullet_scene.instantiate()
	var marker_pos
	if follow_parent:
		add_child(bullet)
		marker_pos = parent.get_node("MarkerShoot").position
	else:
		parent.parent.add_child(bullet)
		bullet.APShoot.play()
		marker_pos = parent.position + parent.get_node("MarkerShoot").position
	bullet.change_position(marker_pos)
	if bulletType == BulletTypes.Bullet:
		bullet.movement_speed = bullet_speed

func _on_timer_shoot_timeout():
	can_shoot = true

func _on_timer_shoot_ray_timeout():
	Shoot()
	shoot = true
