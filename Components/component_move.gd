extends Node2D
class_name ComponentMove

@export var move: bool = true
@export var moveInfinitely: bool = false
@export var iaMovementY: bool = false
@export var move_to_target: bool = false
@export var speed: float = 300.0
@export var current_direction: Direction = Direction.LEFT
@export var moving_time: float = 0.4

enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT }
var movement: Vector2 = Vector2.ZERO
var direction: Vector2 = Vector2.ZERO

var vel_ia: float = 150.0
var movement_y: Vector2 = Vector2.UP
var change_movement_direction_y: float = 165.0

@onready var parent = get_parent()
@onready var rb = parent.get_node("RigidBody2D")
@onready var CShoot
@onready var timerMove = $TimerMove
@onready var target = parent.get_parent().get_node("Nave")

func _ready():
	if parent.has_node("Component Shoot"):
		CShoot = parent.get_node("Component Shoot")
	rb.position = position
	timerMove.wait_time = moving_time
	ChangeDirection(current_direction)

func _process(delta):
	if move:
		Move(delta)
	elif iaMovementY:
		IaMovementY(delta)
	elif move_to_target:
		if parent.get_parent().has_node("Nave"):
			MoveTowardsTarget(target.position, delta)

func Move(delta):
	movement = direction * speed * delta
	rb.move_and_collide(movement)
	parent.position = rb.position

func IaMovementY(delta):
	movement_y = movement_y.normalized() * vel_ia * delta
	rb.move_and_collide(movement_y)
	parent.position = rb.position
	if parent.position.y > change_movement_direction_y or parent.position.y < -change_movement_direction_y:
		change_direction_y()

func MoveTowardsTarget(target_position, delta):
	var direction_to_target = (target_position - parent.position).normalized()
	movement = direction_to_target * speed * delta
	rb.move_and_collide(movement)
	parent.position = rb.position

func change_direction_y():
	movement_y.y *= -1

func change_position(pos):
	position = pos
	rb.position = position

func ChangeDirection(dir):
	if dir == Direction.UP:
		direction = Vector2.UP
	elif dir == Direction.DOWN:
		direction = Vector2.DOWN
	elif dir == Direction.LEFT:
		direction = Vector2.LEFT
	elif dir == Direction.RIGHT:
		direction = Vector2.RIGHT

func StartShooting():
	if parent.has_node("Component Shoot"):
		if not CShoot.start_shooting:
			CShoot.start_shooting = true
			CShoot.can_shoot = true
		if CShoot.bulletType == CShoot.BulletTypes.Ray:
			CShoot.get_node("TimerShootRay").start()
			CShoot.shoot = true

func _on_timer_move_timeout():
	if not moveInfinitely:
		move = false
		StartShooting()
