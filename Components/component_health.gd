extends Node2D
class_name ComponentHealth

@export var health: int = 3
@export var timeInvulnerable: float = 1.25
@export var timeBlink: float = 0.16
@export var blink: bool = true
@export var hit: bool = false
@export var deathAnimation: bool = true
@export var changeSprite: bool = false
@export var sprites = []

var is_vulnerable: bool = true
var death_animation_started: bool = false
var killed_by_player: bool = false

@onready var parent = get_parent()
@onready var timerInvulnerable = $TimerInvulnerable
@onready var timerBlink = $TimerBlink

func _ready():
	timerInvulnerable.wait_time = timeInvulnerable
	timerBlink.wait_time = timeBlink

func _process(_delta):
	if death_animation_started and not parent.get_node("Sprite2D/AnimationPlayer").is_playing():
		parent.queue_free()

func TakeDamage():
	health -= 1
	
	if health > 0:
		is_vulnerable = false
		timerInvulnerable.start()
		if changeSprite:
			parent.get_node("Sprite2D").texture = sprites[health]
		if blink:
			call_deferred("Blink")
		if hit:
			parent.get_node("Sprite2D/AnimationPlayer").play()
	else:
		Die()

func Die():
	if deathAnimation:
		parent.get_node("Sprite2D/AnimationPlayer").play("Death")
		death_animation_started = true
	else: 
		parent.queue_free()
		if parent.has_node("Component Shoot"):
			if parent.get_node("Component Shoot").has_animation:
				parent.get_node("Sprite2D/AnimationPlayer").speed_scale = 1
	killed_by_player = true
	if parent.has_node("Component Shoot"):
		parent.get_node("Component Shoot").timerShoot.stop()

func Blink():
	for i in range(3):
		parent.visible = false
		timerBlink.start()
		await timerBlink.timeout
		parent.visible = true
		await timerBlink.timeout
		timerBlink.stop()

func _on_timer_invulnerable_timeout():
	is_vulnerable = true
